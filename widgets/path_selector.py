import tkinter as tk
from tkinter import filedialog
from enum import Enum


class PathTypes(Enum):
    FILE = 0,
    DIRECTORY = 1


class PathSelector(tk.Frame):
    def __init__(self, parent, selector_text,  initial_path, path_type, on_uploaded=None):
        super(PathSelector, self).__init__(parent)
        self.grid_rowconfigure(0, weight=1)
        self.grid_rowconfigure(1, weight=1)
        self.grid_columnconfigure(0, weight=1)

        self.path_type = path_type
        self.on_uploaded = on_uploaded

        self.button = tk.Button(
            self,
            text=selector_text,
            font=('Arial', 20),
            bg='#023e8a',
            fg='#ffffff',
            borderwidth=0,
            command=self._on_click
        )
        self.button.grid(row=0, column=0, padx=5, pady=5, sticky='nsew')

        self.path = tk.StringVar(value=initial_path)
        tk.Entry(
            self,
            textvariable=self.path,
            state=tk.DISABLED,
            disabledforeground='#000'
        ).grid(row=1, column=0, padx=5, pady=5, sticky='nsew')

    def get_path(self):
        return self.path.get()

    def _on_click(self):
        if self.path_type == PathTypes.FILE:
            path = filedialog.askopenfilename()
        else:
            path = filedialog.askdirectory()

        if path:
            self.path.set(path)
            self.on_uploaded(
                self.get_path()
            )
