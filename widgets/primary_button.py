import tkinter as tk


class PrimaryButton(tk.Frame):
    def __init__(self, parent, **kwargs):
        super(PrimaryButton, self).__init__(parent)

        self.grid_rowconfigure(0, weight=1)
        self.grid_columnconfigure(0, weight=1)

        self.button = tk.Button(
            self,
            font=('Arial', 20),
            bg='#023e8a',
            fg='#ffffff',
            borderwidth=0
        )
        self.button.config(kwargs)
        self.button.grid(row=0, column=0, padx=5, pady=5, sticky='nsew')

    def set_attr(self, **attr_dict):
        self.button.config(attr_dict)

    def get_attr(self, attr: str) -> str:
        return self.button[attr]
