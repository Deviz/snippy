import tkinter as tk


class SimpleSwitchSettings:
    ACTIVE_COLOR = '#fb8500'
    INACTIVE_COLOR = '#ffb703'


class SimpleSwitch(tk.Frame):
    def __init__(self, parent, inactive_title, active_title, on_switch=None, default=False):
        super(SimpleSwitch, self).__init__(parent)

        self.grid_rowconfigure(0, weight=1)
        self.grid_columnconfigure(0, weight=1)

        self.is_active = not default
        self.inactive_title = inactive_title
        self.active_title = active_title
        self.on_switch = on_switch

        self.btn = tk.Button(
            self,
            text=inactive_title,
            font=('Arial', 20),
            bg=SimpleSwitchSettings.INACTIVE_COLOR,
            borderwidth=0,
            fg='#ffffff',
            command=self.toggle
        )
        self.btn.grid(
            row=0,
            column=0,
            padx=5,
            pady=5,
            sticky='nsew'
        )

        # Initialize default value
        self.toggle()

    def toggle(self):
        self.is_active = not self.is_active

        if self.is_active:
            self.btn.config(
                bg=SimpleSwitchSettings.ACTIVE_COLOR,
                text=self.active_title
            )
        else:
            self.btn.config(
                bg=SimpleSwitchSettings.INACTIVE_COLOR,
                text=self.inactive_title
            )

        self.on_switch(self.is_active)
