import tkinter as tk


class ModeSwitchSettings:
    ACTIVE_COLOR = '#fb8500'
    INACTIVE_COLOR = '#ffb703'


class ModeSwitch(tk.Frame):
    """
    :param parent : tk.Frame or root window
    :param modes : Enum (must be text enum)
    :param on_switch : Callable optional event handler function
    :param default : Enum optional switch default selection
    """
    def __init__(self, parent, modes, on_switch=None, default=None):
        super(ModeSwitch, self).__init__(parent)
        self.on_switch = on_switch
        self.last_clicked_title = default.value or list(modes)[0].value

        self.modes = {
            title: Mode(self, i, title, lambda t=title: self.on_click(t))
            for i, title in enumerate([m.value for m in modes])
        }

        self.grid_rowconfigure(0, weight=1)
        for i in range(len(self.modes)):
            self.grid_columnconfigure(i, weight=1)

        self.modes[self.last_clicked_title].toggle()

    def click_next(self):
        """
        Automatic switching from left to right in circular order
        """
        keys = list(self.modes.keys())

        if self.last_clicked_title == keys[-1]:
            self.on_click(keys[0])
        else:
            self.on_click(keys[keys.index(self.last_clicked_title)+1])

    def get_active(self) -> str:
        """
        :returns last_clicked_title: str
        """
        return self.last_clicked_title

    def on_click(self, mode_title):
        if mode_title == self.last_clicked_title:
            return

        self.modes[self.last_clicked_title].toggle()
        self.modes[mode_title].toggle()

        self.last_clicked_title = mode_title
        self.on_switch()


class Mode:
    def __init__(self, frame, index, title, on_click):
        self.is_active = False
        self.title = title

        self.btn = tk.Button(
            frame,
            text=title,
            font=('Arial', 15),
            bg=ModeSwitchSettings.INACTIVE_COLOR,
            borderwidth=0,
            fg='#ffffff',
            command=lambda: on_click(self.title)
        )
        self.btn.grid(
            row=0,
            column=index,
            padx=5,
            pady=5,
            sticky='nsew'
        )

    def toggle(self):
        self.is_active = not self.is_active

        color = ModeSwitchSettings.ACTIVE_COLOR if self.is_active else ModeSwitchSettings.INACTIVE_COLOR
        self.btn.config(bg=color)
