import tkinter as tk
from pages_manager import PagesManager
from pages import *
from data_manager import DataManager
import sys

assert sys.version_info >= (3, 8)

data_manager = DataManager(
    {
        'tesseract_exe': 'C:\\Program Files\\Tesseract-OCR\\tesseract.exe',
        'default_mode': 'Image mode',
        'base_snips_dir': 'snips',
        'show_processed_image': False,
        'keybinds': {  # Keybinds order matters!
            'toggle_system_tray': '<ctrl>+<alt>+q',
            'text_capture': '<ctrl>+<alt>+t',
            'image_capture': '<ctrl>+<alt>+i',
            'switch_modes': '<ctrl>+<alt>+s'
        }
    }
)


class App(tk.Tk):
    def __init__(self):
        super(App, self).__init__()
        self.rowconfigure(0, weight=1)
        self.columnconfigure(0, weight=1)

        PagesManager(
            self,
            (MainPage, SettingsPage)
        )

        self.title('Snippy')
        self.iconbitmap('resources/favicon.ico')
        self.minsize(500, 300)  # This might change after deiconify


if __name__ == '__main__':
    App().mainloop()
