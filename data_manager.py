import json
import os


class DataManager:
    def __init__(self, default_data: dict, path='data.json'):
        self.path = path

        if not os.path.isfile(path):  # Checking if file exists
            self.update_data(default_data)  # Saving default_data to file

        self.data = self.get_data()

        # Create data file for non-existing keys
        for key, val in default_data.items():
            if key not in self.data.keys():
                self.set(key, val)

    def get_data(self) -> dict:
        with open(self.path, 'r') as json_file:
            return json.load(json_file)

    def update_data(self, data: dict = None):
        """
        Writes to json file data property
        :param data: dict
        """
        if data is None:
            data = self.data

        with open(self.path, 'w') as json_file:
            json.dump(data, json_file)

    def set(self, attr: str, val: str):
        try:
            self.data[attr] = val
            self.update_data(self.data)
        except KeyError:
            raise Exception('DataManager: Key "' + attr + '" does not exist.')

    def get(self, attr: str):
        try:
            return self.data[attr]
        except KeyError:
            raise Exception('DataManager: Key "' + attr + '" does not exist.')
