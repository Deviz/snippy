class PagesManager:
    def __init__(self, parent, pages: tuple):
        """
        :param parent: tk.Tk or tk.Frame
        :param pages: tuple of child page classes of a tk.Frame
        """
        self._pages = {}
        self._parent = parent

        for F in pages:
            self.add_page(F)

        # Showing first page from initial pages tuple
        self.show_page(
            pages[0].__name__
        )

    def add_page(self, page):
        frame = page(root=self._parent, manager=self)
        frame.grid(row=0, column=0, sticky='nsew')
        # Adding page to the self.pages dictionary
        self._pages[page.__name__] = frame

    def get_page(self, name):
        return self._pages[name]

    def show_page(self, name):
        page = self.get_page(name)
        # Show frame
        page.tkraise()
        # Every page must have this event handler
        page.on_page_show()
