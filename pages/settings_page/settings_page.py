import tkinter as tk
from widgets import PrimaryButton, PathSelector, PathTypes, SimpleSwitch
from main import data_manager
from .keybinds_page import KeyBindsPage


class SettingsPage(tk.Frame):
    def __init__(self, root, manager):
        super(SettingsPage, self).__init__()
        self.root = root
        manager.add_page(KeyBindsPage)

        [self.grid_rowconfigure(i, weight=1) for i in range(5)]
        self.grid_columnconfigure(0, weight=1)

        SimpleSwitch(
            self,
            inactive_title='Processed image will not be shown',
            active_title='Processed image will be shown',
            on_switch=lambda is_active: data_manager.set('show_processed_image', is_active),
            default=data_manager.get('show_processed_image')
        ).grid(row=0, column=0, sticky='nsew')

        PathSelector(
            self,
            selector_text='Choose base directory',
            initial_path=data_manager.get('base_snips_dir'),
            path_type=PathTypes.DIRECTORY,
            on_uploaded=lambda path: data_manager.set('base_snips_dir', path)
        ).grid(row=1, column=0, sticky='nsew')

        PathSelector(
            self,
            selector_text='Choose tesseract.exe',
            initial_path=data_manager.get('tesseract_exe'),
            path_type=PathTypes.FILE,
            on_uploaded=lambda path: data_manager.set('tesseract_exe', path)
        ).grid(row=2, column=0, sticky='nsew')

        PrimaryButton(
            self,
            text='KeyBinds settings',
            command=lambda: manager.show_page('KeyBindsPage')
        ).grid(row=3, column=0, sticky='nsew')

        PrimaryButton(
            self,
            text='Go back',
            bg='#03045e',
            command=lambda: manager.show_page('MainPage')
        ).grid(row=4, column=0, sticky='nsew')

    def on_page_show(self):
        pass
