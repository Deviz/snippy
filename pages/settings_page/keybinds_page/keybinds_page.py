import tkinter as tk
import enum
from tkinter import filedialog, messagebox
from widgets import PrimaryButton
from main import data_manager
from pynput import keyboard


class KeyBindsPage(tk.Frame):
    def __init__(self, root, manager):
        super(KeyBindsPage, self).__init__()
        self.root = root
        self.manager = manager

        self.grid_columnconfigure(0, weight=1)
        self.grid_rowconfigure(0, weight=4)
        self.grid_rowconfigure(1, weight=1)

        # TODO: make those boys more abstract
        self.non_printable = {
            r"'\x01'": 'A',
            r"'\x02'": 'B',
            r"'\x03'": 'C',
            r"'\x04'": 'D',
            r"'\x05'": 'E',
            r"'\x06'": 'F',
            r"'\x07'": 'G',
            r"'\x08'": 'H',
            r"'\x09'": 'I',
            r"'\x0a'": 'J',
            r"'\x0b'": 'K',
            r"'\x0c'": 'L',
            r"'\x0d'": 'M',
            r"'\x0e'": 'N',
            r"'\x0f'": 'O',
            r"'\x10'": 'P',
            r"'\x11'": 'Q',
            r"'\x12'": 'R',
            r"'\x13'": 'S',
            r"'\x14'": 'T',
            r"'\x15'": 'U',
            r"'\x16'": 'V',
            r"'\x17'": 'W',
            r"'\x18'": 'X',
            r"'\x19'": 'Y',
            r"'\x1a'": 'Z',
            r"'\x1b'": '[',
            r"'\x1d'": ']',
            r"'\x1f'": '-',
        }
        self.conversion_elements = ['!', '@', '#', '$', '%', '^', '&', '*', '(', ')']
        self.invalid_shift_combinations = ['{', '}', '|', '_', '"', '<', '>', '?', '~']

        self.curr_key_bind = list()
        self.last_key_bind = None
        self.curr_listening = None
        self.listener = None

        wrapper = tk.Frame(self)
        wrapper.grid(row=0, column=0, sticky='nsew')
        wrapper.grid_columnconfigure(0, weight=1)
        wrapper.grid_columnconfigure(1, weight=1)

        self.key_binds = data_manager.get('keybinds')
        self.buttons = list()
        for i, (key, value) in enumerate(self.key_binds.items()):
            tk.Label(wrapper, text=key.upper()).grid(row=i, column=0, sticky='nsew')
            self.buttons.append(
                PrimaryButton(
                    wrapper,
                    font=('Arial', 10),
                    width=5,
                    text=self.key_binds[key],
                    command=lambda index=i, title=key: self.setup_key_bind(index, title)
                )
            )
            self.buttons[i].grid(row=i, column=1, sticky='nsew')
            wrapper.grid_rowconfigure(i, weight=1)

        PrimaryButton(
            self,
            text='Go back',
            bg='#03045e',
            command=lambda: manager.show_page('SettingsPage')
        ).grid(row=1, column=0, sticky='nsew')

    def setup_key_bind(self, index, title):
        if self.curr_listening is None:
            self.listener = keyboard.Listener(on_press=self.on_press, on_release=self.on_release)
            self.listener.start()
            self.curr_listening = [index, title]
            self.last_key_bind = self.key_binds[title]

            self.buttons[index].set_attr(text='Recording...')
            self.disable_buttons_except(index)
            self.manager.get_page('MainPage').stop_listener()

    def disable_buttons_except(self, except_index):
        for i, btn in enumerate(self.buttons):
            if i != except_index:
                btn.set_attr(state=tk.DISABLED)

    def enable_buttons(self):
        for i, btn in enumerate(self.buttons):
            btn.set_attr(state=tk.NORMAL)

    def reset(self, error=False):
        if error:
            self.buttons[self.curr_listening[0]].set_attr(text=self.last_key_bind)

        self.enable_buttons()
        self.curr_listening = None
        self.listener.stop()
        self.listener = None
        self.curr_key_bind.clear()

        # Reload hotkey listener in main page
        self.manager.get_page('MainPage').init_listener(self.key_binds)

    def on_release(self, key):
        if len(self.curr_key_bind) > 5:
            messagebox.showerror('Error!', 'Why would you want to use more than five fingers?')
        elif len(self.curr_key_bind) <= 4 and self.curr_listening is not None:
            try:
                key_bind = '+'.join(self.curr_key_bind)
            except TypeError:
                messagebox.showerror('Error!', 'Invalid combination!')
                self.reset(error=True)
                return

            key_binds = self.key_binds.copy()
            key_binds[self.curr_listening[1]] = key_bind

            # Check if new key combination is unique since set can hold only unique elements
            if len(key_binds) != len(set(key_binds.values())):
                messagebox.showerror('Error!', 'This combination already exists!')
                self.reset(error=True)
                return
            elif self.is_invalid_combination(key_bind):
                # I know, this is redundant (will be fixed soon)
                messagebox.showerror('Error!', 'Invalid combination!')
                self.reset(error=True)
                return

            # Updating dictionary if passed all tests
            self.buttons[self.curr_listening[0]].set_attr(text=key_bind)
            self.key_binds = key_binds

            # Save keybinder data to file
            data_manager.set('keybinds', self.key_binds)

            # Finish up
            self.reset()

    def on_press(self, key):
        if len(self.curr_key_bind) >= 4 and self.curr_listening is None:
            return
        # If key is special
        elif isinstance(key, enum.Enum):
            key = key.name.split('_')[0] if '_' in key.name else key.name
            key = '<' + key + '>'
        elif isinstance(key, keyboard.KeyCode):
            if str(key).count('<') == 1 and str(key).count('>') == 1 and 32 < int(str(key)[1:-1]) < 96:
                try:
                    key = chr(int(str(key)[1:-1])).lower()
                    # print('ascii:', chr(int(str(key)[1:-1])).lower())
                except ValueError:
                    messagebox.showerror('qError!', f'Invalid combination!')
                    return
            elif str(key).count('\\') == 1 and len(str(key)) > 1:
                # print('non-printable:', str(key))
                if str(key) in self.non_printable:
                    # print(self.non_printable[str(key)])
                    key = self.non_printable[str(key)].lower()
                else:
                    messagebox.showerror('qError!', f'Invalid combination!')
                    return
            elif key.char:
                key = key.char
                if key in self.conversion_elements:
                    index = self.conversion_elements.index(key) + 1
                    key = '0' if index == 10 else str(index)
                # print('char:', key)

        if key is None:
            messagebox.showerror('Error!', f'Unknown combination!')
        elif key not in self.curr_key_bind:
            self.curr_key_bind.append(key)

        # print(self.curr_key_bind)

    def is_invalid_combination(self, key_bind):
        if any('<shift>+' + el in key_bind for el in self.invalid_shift_combinations):
            return True
        return False

    @staticmethod
    def save_dir(attr: str):
        data_manager.set(attr, filedialog.askdirectory())

    def on_page_show(self):
        pass
