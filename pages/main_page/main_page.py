import tkinter as tk
import pyperclip
import os
import mss
import mss.tools
import sys
from widgets import PrimaryButton, ModeSwitch
from PIL import ImageTk, Image
from enum import Enum
from recognizer import Recognizer
from main import data_manager
from pynput import keyboard
from infi.systray import SysTrayIcon
from datetime import datetime


class Modes(Enum):
    IMAGE = 'Image mode'
    TEXT = 'Text mode'

    @staticmethod
    def get_dir(mode):
        if mode == Modes.IMAGE:
            return 'image_snips/'
        else:
            return 'text_snips/'

    @staticmethod
    def from_str(label):
        if label == 'Image mode':
            return Modes.IMAGE
        elif label == 'Text mode':
            return Modes.TEXT
        else:
            raise NotImplementedError


class MainPage(tk.Frame):
    def __init__(self, root, manager):
        super(MainPage, self).__init__()
        self.root = root
        root.protocol('WM_DELETE_WINDOW', self.on_window_close)

        self.mode = Modes.from_str(data_manager.get('default_mode'))
        self.tray_mode = False

        self.grid_rowconfigure(0, weight=1)
        self.grid_rowconfigure(1, weight=1)
        self.grid_rowconfigure(2, weight=1)
        self.grid_columnconfigure(0, weight=1)

        PrimaryButton(self, text='Capture', command=self.capture) \
            .grid(row=0, column=0, sticky='nsew')

        self.switch = ModeSwitch(self, modes=Modes, on_switch=self.on_switch, default=self.mode)
        self.switch.grid(row=1, column=0, sticky='nsew')

        PrimaryButton(self, text='Settings', command=lambda: manager.show_page('SettingsPage')) \
            .grid(row=2, column=0, sticky='nsew')

        # TODO: add icons for each mode
        self.sys_tray = SysTrayIcon(
            'resources/favicon.ico',
            'Snippy',
            (
                ('Open / Close', None, lambda st: self.toggle_system_tray()),
                (
                    'Capture',
                    None,
                    (
                        (
                            'Text mode',
                            None,
                            lambda st: self.capture(mode=Modes.TEXT)
                        ),
                        (
                            'Image mode',
                            None,
                            lambda st: self.capture(mode=Modes.IMAGE)
                        ),
                    )
                )
            ),
            default_menu_index=1,
            on_quit=self.on_quit
        )
        self.sys_tray.start()

        self.listener = None
        self.init_listener(data_manager.get('keybinds'))

        if len(sys.argv) > 1 and sys.argv[1] == '--silent':
            self.root.withdraw()
            self.tray_mode = True

    def stop_listener(self):
        if self.listener is not None:
            self.listener.stop()

    def init_listener(self, key_binds):
        self.stop_listener()

        handlers = [
            self.toggle_system_tray,
            lambda: self.capture(mode=Modes.TEXT),
            lambda: self.capture(mode=Modes.IMAGE),
            self.switch.click_next
        ]

        self.listener = keyboard.GlobalHotKeys(
            {list(key_binds.values())[i]: handlers[i] for i in range(len(handlers))}
        )
        self.listener.start()

    def on_window_close(self):
        self.sys_tray.shutdown()
        self.on_quit()

    def on_quit(self, sys_tray=None):
        try:
            self.root.quit()
        except RuntimeError:
            self.root.destroy()

    def toggle_system_tray(self):
        if self.root.winfo_viewable():
            self.root.withdraw()
            self.tray_mode = True
        else:
            self.root.deiconify()
            self.tray_mode = False

    def on_switch(self):
        mode = Modes.from_str(self.switch.get_active())

        if self.switch.get_active() != self.mode:
            self.mode = mode
            data_manager.set('default_mode', mode.value)

    def capture(self, mode=None):
        if mode is not None:
            self.switch.on_click(mode.value)

        self.root.withdraw()
        self.root.after(
            300,
            lambda: Capture(self.root, self)
        )

    def on_page_show(self):
        pass


class Capture:
    BORDER_WIDTH = 2

    def __init__(self, root, parent):
        self.root = root
        self.parent = parent
        self.top_level = tk.Toplevel()

        self.top_level.attributes('-fullscreen', True)
        self.top_level.state('zoomed')

        self.top_level.grid_rowconfigure(0, weight=1)
        self.top_level.grid_columnconfigure(0, weight=1)
        self.canvas = tk.Canvas(self.top_level, cursor='cross', highlightthickness=0)
        self.canvas.grid(row=0, column=0, sticky='nsew')

        # Grabbing current screen
        with mss.mss() as sct:
            sct_img = sct.grab(sct.monitors[1])

        self.image = ImageTk.PhotoImage(
            Image.frombytes('RGB', sct_img.size, sct_img.bgra, 'raw', 'BGRX')
        )
        # Overlaying grabbed image on the screen so we can draw
        self.photo = self.canvas.create_image(0, 0, image=self.image, anchor='nw')

        self.x, self.y = 0, 0
        self.rect, self.start_x, self.start_y = None, None, None

        self.canvas.tag_bind(self.photo, '<ButtonPress-1>', self.on_button_press)
        self.canvas.tag_bind(self.photo, '<B1-Motion>', self.on_move_press)
        self.canvas.tag_bind(self.photo, '<ButtonRelease-1>', self.on_button_release)

    def on_button_press(self, event):
        self.start_x, self.start_y = event.x, event.y
        self.rect = self.canvas.create_rectangle(self.x, self.y, 1, 1, outline='red', width=self.BORDER_WIDTH)

    def on_move_press(self, event):
        cur_x, cur_y = (event.x, event.y)
        self.canvas.coords(self.rect, self.start_x, self.start_y, cur_x, cur_y)

    def on_button_release(self, event):
        bbox = self.canvas.bbox(self.rect)

        bbox = (
            bbox[0] + self.BORDER_WIDTH + 1,
            bbox[1] + self.BORDER_WIDTH + 1,
            bbox[2] - self.BORDER_WIDTH - 1,
            bbox[3] - self.BORDER_WIDTH - 1
        )

        # Wait until rectangle drawing is finished so it doesnt overlap
        self.root.after(
            10,
            lambda: self.finish(bbox)
        )

    def finish(self, bbox):
        base_dir = data_manager.get('base_snips_dir')
        if not os.path.exists(base_dir):
            os.mkdir(base_dir)

        image = self.capture(
            base_dir + '/' + Modes.get_dir(self.parent.mode),
            bbox
        )

        self.canvas.destroy()

        if not self.parent.tray_mode:
            self.root.deiconify()

        self.top_level.destroy()

        if self.parent.mode == Modes.TEXT:
            TextWindow(image)

    @staticmethod
    def capture(full_dir, bbox, ext='png'):
        date = datetime.now().strftime('%Y-%m-%d_%H-%M-%S_%f')

        if not os.path.exists(full_dir):
            os.mkdir(full_dir)

        path = full_dir + 'snip-' + date + '.' + ext

        with mss.mss() as sct:
            area = {
                'top': bbox[1],
                'left': bbox[0],
                'width': bbox[2]-bbox[0],
                'height': bbox[3]-bbox[1]
            }

            try:
                image = sct.grab(area)
                mss.tools.to_png(image.rgb, image.size, output=path)
            except mss.exception.ScreenShotError:
                # TODO: logging errors
                print('gdi32.GetDIBits() failed.')

        try:
            return image
        except UnboundLocalError:
            print('ScreenshotManager: local variable image referenced before assignment')


class TextWindow:
    def __init__(self, image):
        self.image = image
        self.top_level = tk.Toplevel()
        self.text = 'Processing image...'

        self.top_level.grid_columnconfigure(0, weight=1)
        self.top_level.grid_rowconfigure(0, weight=1)

        self.textarea = tk.Text(
            self.top_level,
            width=60,
            height=20,
            state=tk.DISABLED
        )
        self.textarea.grid(row=0, column=0, pady=5, padx=5, sticky='nsew')

        self.button = PrimaryButton(
            self.top_level,
            text='Copy text',
            command=lambda: self.copy()
        )
        self.button.grid(row=1, column=0, sticky='nsew')

        self.text = Recognizer(
            image,
            data_manager.get('tesseract_exe')
        ).get_text()

        self.insert_text(
            self.text
        )

    def copy(self):
        pyperclip.copy(self.text)
        self.button.set_attr(text='Copied!')

    def insert_text(self, text):
        self.textarea.config(state=tk.NORMAL)
        self.textarea.insert(tk.END, text + '\n')
        self.textarea.config(state=tk.DISABLED)
        self.textarea.see(tk.END)
