# Snippy
##### Tool for selected area screenshot text conversion

### Dependencies
Only Windows platform is supported for now.  
Python version >= 3.8

- pillow~=8.0.1
- numpy~=1.19.3
- opencv-contrib-python-headless~=4.4.0.46
- pytesseract~=0.3.6
- pyperclip~=1.8.1
- mss~=6.1.0
- matplotlib~=3.3.3
- pynput~=1.6.8
- infi.systray~=0.1.12

### How to setup?
- Install Tesseract-OCR: http://digi.bib.uni-mannheim.de/tesseract/tesseract-ocr-setup-4.00.00dev.exe
- `pip install --upgrade --force-reinstall -r requirements.txt`
- `python main.py`
- Set tesseract.exe path in case it's empty in settings page

### Possible issues and solutions
- `ImportError: numpy.core.multiarray failed to import`  
 To solve this issue execute those commands:  
 `pip uninstall numpy`  
 `pip install numpy==1.19.3`
- `ImportError: Failed to execute main script`  
 To solve this issue execute those commands:  
 `pip uninstall pynput`  
 `pip install pynput==1.6.8`
- If you get similar error be sure to check that requirements.txt versions match 
installed packages (`pip list`) versions. If anything doesn't - reinstall 
correct version using analogy in first step.
 
 ### Advanced setup
 Before starting this make sure you completed "How to setup?" part and everything works.  
 - `pip install pyinstaller`
 - `pyinstaller --noconfirm --onefile --windowed --icon "resources/favicon.ico" --name "snippy" "main.py"`
 - Download "Inno Setup Compiler" from https://jrsoftware.org/isdl.php
 - Open snippy.iss with "Inno Setup Compiler" and press run (`F9`)
 - Now snippy-setup.exe has been generated into `Output` folder