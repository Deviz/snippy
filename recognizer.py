import cv2
import pytesseract
import numpy as np
import os
import matplotlib.pyplot as plt
import gc
from main import data_manager


class Preprocessor:
    def __init__(self, image):
        self.image = image

    def process(self):
        if self.image is None:
            raise Exception('Preprocessor: image is null!')

        # Grayscale image
        gray = cv2.cvtColor(self.image, cv2.COLOR_BGR2GRAY)
        # Upscaled
        sr = self.super_resolution(gray)
        # Thresholding, screenshot converting to black and white
        # using Otsu and Binary inversion
        thresh = self.rotate(
            cv2.threshold(sr, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)[1]
        )

        # Black becomes white and white becomes black
        invert = 255 - thresh

        if self.is_background_black(thresh):
            return invert

        return thresh

    @staticmethod
    def rotate(thresh):
        """
        :param thresh Must be white text in black background
        """
        coordinates = np.column_stack(
            np.where(thresh > 0)
        )
        # Returns in range [-90, 0)
        angle = cv2.minAreaRect(coordinates)[-1]

        if angle < -45:
            angle = -(90 + angle)
        else:
            angle = -angle

        (h, w) = thresh.shape[:2]
        center = (w // 2, h // 2)

        r_mat = cv2.getRotationMatrix2D(center, angle, 1.0)
        rotated = cv2.warpAffine(
            thresh,
            r_mat,
            (w, h),
            flags=cv2.INTER_CUBIC,
            borderMode=cv2.BORDER_REPLICATE
        )

        return rotated

    @staticmethod
    def super_resolution(gray):
        """
        :param gray Must be grayscaled image
        """
        sr = cv2.dnn_superres.DnnSuperResImpl_create()
        sr.readModel('resources/ESPCN_x4.pb')
        sr.setModel('espcn', 4)
        return sr.upsample(gray)

    @staticmethod
    def is_background_black(thresh):
        w, h = thresh.shape[:2]  # Select half image

        # Non zero is white color
        if cv2.countNonZero(thresh) > ((w * h) // 2):  # Background is white
            return False

        return True


class Recognizer:
    """
    Install tesseract from here:
    http://digi.bib.uni-mannheim.de/tesseract/tesseract-ocr-setup-4.00.00dev.exe
    """
    def __init__(self, image, tesseract_exe=None):
        self.image = np.array(image)
        self.debug = data_manager.get('show_processed_image')

        default_paths = [
            'C:\\Program Files\\Tesseract-OCR\\tesseract.exe',
            'C:\\Program Files (x86)\\Tesseract-OCR\\tesseract.exe',
            tesseract_exe
        ]

        found = False
        for path in default_paths:
            if os.path.isfile(path):
                pytesseract.pytesseract.tesseract_cmd = path
                data_manager.set('tesseract_exe', path)
                found = True
                break

        if not found:
            raise Exception(f'Tesseract executable was not found! ')

    def get_text(self):
        processed = Preprocessor(self.image).process()

        if self.debug:
            fig = plt.figure('Processed image')
            fig.canvas.mpl_connect('close_event', lambda e: gc.collect())
            plt.title('Processed image')
            plt.imshow(processed, cmap='gray')
            plt.show(block=False)

        data = pytesseract.image_to_string(
            processed,
            lang='eng',
            config='--psm 3 --oem 3'
        )

        return data
